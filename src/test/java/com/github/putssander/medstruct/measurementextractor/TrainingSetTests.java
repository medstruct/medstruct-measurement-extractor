package com.github.putssander.medstruct.measurementextractor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.putssander.medstruct.measurementextractor.measurement.MeasurementExpression;
import com.github.putssander.medstruct.measurementextractor.service.MeasurementExtractorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TrainingSetTests {

    private static String case14 = "src/test/resources/ignored/14.json";

    @Autowired
    private MeasurementExtractorService measurementExtractorService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void extractMeasurementsCase14() throws IOException {
        String jsonNlp = new String(Files.readAllBytes(Paths.get(case14)));
        JsonNode jsonNlpNode = objectMapper.readTree(jsonNlp);
        JsonNode document = jsonNlpNode.get("documents").get(0);


        List<MeasurementExpression> measurements = measurementExtractorService.extractMeasurements(document);


//        boolean foundMeasurementLoincValue = false;
//        BigDecimal codeValue = BigDecimal.valueOf(7);
//        for(MeasurementExpression measurement : measurements){
//            if(measurement.getCodeValue() != null && measurement.getCodeValue() == codeValue)
//                foundMeasurementLoincValue = true;
//        }
//
//        assertTrue(foundMeasurementLoincValue);
//
//        document = measurementExtractorService.addMeasurements(document, measurements);
////		((ObjectNode) jsonNlpNode).putArray("documents").add(document);
//        ((ArrayNode) jsonNlpNode.withArray("documents")).set(0, document);
//        String jsonNlpResult = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNlpNode);
//        try (PrintStream out = new PrintStream(new FileOutputStream(jsonNlpDocResultPath))) {
//            out.print(jsonNlpResult);
//        }
    }


}
