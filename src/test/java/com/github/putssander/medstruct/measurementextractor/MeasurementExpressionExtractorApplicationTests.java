package com.github.putssander.medstruct.measurementextractor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.putssander.medstruct.measurementextractor.measurement.MeasurementExpression;
import com.github.putssander.medstruct.measurementextractor.service.MeasurementExtractorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MeasurementExpressionExtractorApplicationTests {

	private static final Logger logger = LoggerFactory.getLogger(MeasurementExpressionExtractorApplicationTests.class);

	private static final String jsonNlpDocPath = "src/test/resources/json-nlp.json";
	private static final String jsonNlpDocResultPath = "src/test/resources/json-nlp-result.json";

	private static final String jsonNlpT2DocPath = "src/test/resources/json-nlp-t2-inv-main-broch.json";
	private static final String jsonNlpT2DocResultPath = "src/test/resources/json-nlp-t2-inv-main-broch-result.json";

	@Autowired
	private MeasurementExtractorService measurementExtractorService;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void contextLoads() {
	}


	@Test
	public void extractTumorSize() throws IOException {
		String jsonNlp = new String(Files.readAllBytes(Paths.get(jsonNlpDocPath)));
		JsonNode jsonNlpNode = objectMapper.readTree(jsonNlp);
		JsonNode document = jsonNlpNode.get("documents").get(0);


		List<MeasurementExpression> measurements = measurementExtractorService.extractMeasurements(document);

		boolean foundMeasurementLoincValue = false;
		BigDecimal codeValue = BigDecimal.valueOf(7);
		for(MeasurementExpression measurement : measurements){
			if(measurement.getCodeValue() != null && measurement.getCodeValue() == codeValue)
				foundMeasurementLoincValue = true;
		}

		assertTrue(foundMeasurementLoincValue);

		document = measurementExtractorService.addMeasurements(document, measurements);
//		((ObjectNode) jsonNlpNode).putArray("documents").add(document);
		((ArrayNode) jsonNlpNode.withArray("documents")).set(0, document);
		String jsonNlpResult = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNlpNode);
		try (PrintStream out = new PrintStream(new FileOutputStream(jsonNlpDocResultPath))) {
			out.print(jsonNlpResult);
		}
	}

	@Test
	public void extractT2() throws IOException {
		String jsonNlp = new String(Files.readAllBytes(Paths.get(jsonNlpT2DocPath)));
		JsonNode jsonNlpNode = objectMapper.readTree(jsonNlp);
		JsonNode document = jsonNlpNode.get("documents").get(0);


		List<MeasurementExpression> measurements = measurementExtractorService.extractMeasurements(document);

		document = measurementExtractorService.addMeasurements(document, measurements);
//		((ObjectNode) jsonNlpNode).putArray("documents").add(document);
		((ArrayNode) jsonNlpNode.withArray("documents")).set(0, document);
		String jsonNlpResult = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNlpNode);
		try (PrintStream out = new PrintStream(new FileOutputStream(jsonNlpT2DocResultPath))) {
			out.print(jsonNlpResult);
		}
	}


}
