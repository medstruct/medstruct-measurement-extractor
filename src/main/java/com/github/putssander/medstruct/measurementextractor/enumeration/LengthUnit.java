package com.github.putssander.medstruct.measurementextractor.enumeration;

public enum LengthUnit {

    m, dm, cm, mm, µm, nm, pm

}
