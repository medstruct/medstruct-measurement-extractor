package com.github.putssander.medstruct.measurementextractor.properties;

import com.github.putssander.medstruct.measurementextractor.enumeration.PhysicalQuantityType;

public class PhysicalQuantitySubtype {

    private PhysicalQuantityType quantity;

    private String subtype;

    private String regex;

    public PhysicalQuantityType getQuantity() {
        return quantity;
    }

    public void setQuantity(PhysicalQuantityType quantity) {
        this.quantity = quantity;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }
}
