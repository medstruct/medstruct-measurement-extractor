package com.github.putssander.medstruct.measurementextractor.web.controller;

import com.github.putssander.medstruct.measurementextractor.service.MeasurementExtractorService;
import com.github.putssander.medstruct.measurementextractor.service.MeasurementProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.*;

@RestController
@ConditionalOnProperty(prefix="restcontroller", name="enabled")
@RequestMapping("/api")
public class MeasurementController {


    private static final Logger logger = LoggerFactory.getLogger(MeasurementProcessor.class);

    private MeasurementExtractorService measurementExtractorService;

    public MeasurementController(MeasurementExtractorService measurementExtractorService) {
        logger.info("restcontroller.enabled=true");
        this.measurementExtractorService = measurementExtractorService;
    }

    @PostMapping("/json-nlp/measurements")
    public String getMeasuments(@RequestBody String jsonNlp){
        logger.info("INPUT");
        jsonNlp = this.measurementExtractorService.extractMeasurements(jsonNlp);
        logger.info("OUTPUT");
        return jsonNlp;
    }
}
