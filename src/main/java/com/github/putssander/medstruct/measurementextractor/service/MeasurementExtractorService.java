package com.github.putssander.medstruct.measurementextractor.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.putssander.medstruct.measurementextractor.enumeration.LengthUnit;
import com.github.putssander.medstruct.measurementextractor.measurement.MeasurementExpression;
import com.github.putssander.medstruct.measurementextractor.properties.CodeProperties;
import com.github.putssander.medstruct.measurementextractor.properties.MeasurementExtractorProperties;
import com.github.putssander.medstruct.measurementextractor.properties.PhysicalQuantitySubtype;
import com.github.putssander.medstruct.measurementextractor.properties.QuantityLengthProperties;
import com.github.putssander.medstruct.measurementextractor.util.JsonNlpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.github.putssander.medstruct.measurementextractor.enumeration.LengthUnit.*;
import static com.github.putssander.medstruct.measurementextractor.enumeration.PhysicalQuantityType.LENGTH;

@Service
@EnableConfigurationProperties({QuantityLengthProperties.class, MeasurementExtractorProperties.class})
public class MeasurementExtractorService {

    private static final Logger logger = LoggerFactory.getLogger(MeasurementExtractorService.class);

    private QuantityLengthProperties quantityLengthProperties;

    private MeasurementExtractorProperties measurementExtractorProperties;

    private ObjectMapper objectMapper;

    private String unitPattern;

    private CodeExtractionService codeExtractionService;

    private Pattern dimensionPattern;


    public MeasurementExtractorService(QuantityLengthProperties quantityLengthProperties,
                                       MeasurementExtractorProperties measurementExtractorProperties,
                                       CodeExtractionService codeExtractionService, ObjectMapper objectMapper) {
        this.quantityLengthProperties = quantityLengthProperties;
        this.measurementExtractorProperties = measurementExtractorProperties;
        this.objectMapper = objectMapper;
        this.codeExtractionService = codeExtractionService;
        initUnitPattern();
    }

    public JsonNode addMeasurements(JsonNode document, List<MeasurementExpression> measurementExpressions){
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode array = mapper.valueToTree(measurementExpressions);
        ((ObjectNode) document).putArray("measurements").addAll(array);
        return document;
    }

    public String extractMeasurements(String jsonNlp){
        try {
            JsonNode jsonNlpNode = objectMapper.readTree(jsonNlp);
            JsonNode document = jsonNlpNode.get("documents").get(0);
            List<MeasurementExpression> measurements = extractMeasurements(document);
            document = addMeasurements(document, measurements);
            ((ArrayNode) jsonNlpNode.withArray("documents")).set(0, document);
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNlpNode);
        } catch (IOException e) {
            logger.error("Error parsing document={}", jsonNlp, e);
        }
        return jsonNlp;
    }

    public List<MeasurementExpression> extractMeasurements(JsonNode document){
        List<MeasurementExpression> measurementExpressions = new ArrayList<>();
        Iterator<JsonNode> sentences = document.get("sentences").elements();

        while (sentences.hasNext()){
            JsonNode sentence = sentences.next();
            Iterator<JsonNode> tokens = getTokens(document, sentence);
            measurementExpressions.addAll(getMeasurementExpressions(tokens));
        }

        int id = 1;
        for(MeasurementExpression measurementExpression : measurementExpressions) {
            measurementExpression.setId(id);
            codeExtractionService.getMeasurementCode(measurementExpression, document);
            id++;
        }

        measurementExpressions.forEach(measurementExpression -> setMeasurementQuantities(document, measurementExpression));
        measurementExpressions = measurementExpressions.stream().filter(measurementExpression -> !measurementExpression.getValues().isEmpty()).collect(Collectors.toList());
        measurementExpressions.forEach(measurementExpression -> normalizeMeasurementLength(measurementExpression));

        return measurementExpressions;
    }


    private List<MeasurementExpression> getMeasurementExpressions(Iterator<JsonNode> tokens){

        List<MeasurementExpression> measurementExpressions = new ArrayList<>();
        List<JsonNode> currentExpressionNodes = new ArrayList<>();
        boolean prevTokenNumber = false;
        boolean prevTokenAttribute = false;

        while(tokens.hasNext()){

            if(!prevTokenAttribute && !prevTokenNumber){
                if(!currentExpressionNodes.isEmpty())
                    currentExpressionNodes = new ArrayList<>();
            }

            JsonNode token = tokens.next();
            String upos = token.get("upos").textValue();
            String value = token.get("text").textValue();

            if(upos.equals("NUM") && !prevTokenNumber){
                currentExpressionNodes.add(token);
                prevTokenNumber = true;
                prevTokenAttribute = false;
            }
            else if(dimensionPattern.matcher(value).matches() && (prevTokenNumber || prevTokenAttribute)){
                currentExpressionNodes.add(token);
                prevTokenNumber = false;
                prevTokenAttribute = true;
            }
            else if(prevTokenNumber  && !currentExpressionNodes.isEmpty() && value.matches(unitPattern)){
                currentExpressionNodes.add(token);
                measurementExpressions.add(getMeasurementFromTokens(currentExpressionNodes.iterator()));
                prevTokenNumber = false;
                prevTokenAttribute = false;
            }
            else{
                prevTokenNumber = false;
                prevTokenAttribute = false;
            }

        }
        return measurementExpressions;
    }

    private MeasurementExpression getMeasurementFromTokens(Iterator<JsonNode> tokens) {
        MeasurementExpression measurementExpression = new MeasurementExpression();

        StringBuilder stringBuilder = new StringBuilder();
        while (tokens.hasNext()){
            JsonNode token = tokens.next();
            String text = token.get("text").textValue();
            measurementExpression.getTokens().add(token.get("id").asInt());

            if(!tokens.hasNext()){
                measurementExpression.setExpression(stringBuilder.append(text).toString());
                if(text.matches(quantityLengthProperties.getUnitMmPattern()))
                    measurementExpression.setUnit(mm.toString());
                if(text.matches(quantityLengthProperties.getUnitCmPattern()))
                    measurementExpression.setUnit(cm.toString());
                if(text.matches(quantityLengthProperties.getUnitDmPattern()))
                    measurementExpression.setUnit(dm.toString());
            }
            else{
                stringBuilder.append(token.get("text").textValue());
                if(token.get("misc").get("SpaceAfter").asBoolean() == true){
                    stringBuilder.append(" ");
                }
                if(token.get("upos").textValue().matches("NUM")) {
                    addMeasurementValue(measurementExpression, text);
                }
            }
        }
        return measurementExpression;
    }


    private void addMeasurementValue(MeasurementExpression measurementExpression, String text){
        try {
            if(text.matches(measurementExtractorProperties.getBlackListPattern()))
                return;

            String[] split = text.split(measurementExtractorProperties.getSplitPattern());
            for(String s : split){
                text = s.replaceAll(",", ".").toLowerCase();
                for(Map.Entry<String, String> entry : measurementExtractorProperties.getOrdinalMap().entrySet()){
                    if(text.contains(entry.getValue()))
                        text = text.replaceAll(entry.getValue(), entry.getKey());
                }
                text = text.replaceAll("[^\\d.]", "");

                if(!text.isEmpty()) {
                    BigDecimal value = new BigDecimal(text);
                    measurementExpression.getValues().add(value);
                }}
        }
        catch (Exception e){
            logger.error("Error adding value for text={}", text, e);
        }
    }

    private Iterator<JsonNode> getTokens(JsonNode document, JsonNode sentence){

        Iterator<JsonNode> tokenIdsElements = sentence.get("tokens").elements();

        List<Integer> tokenIds = new ArrayList<>();
        tokenIdsElements.forEachRemaining(id -> tokenIds.add(id.asInt()));

        List<JsonNode> sentenceTokenList = new ArrayList<>();
        Iterator<JsonNode> tokenList = document.get("tokenList").elements();

        while (tokenList.hasNext()){
            JsonNode token = tokenList.next();
            if(tokenIds.contains(token.get("id").asInt()))
                sentenceTokenList.add(token);
        }
        return sentenceTokenList.iterator();
    }


    private void initUnitPattern(){
        this.unitPattern =  quantityLengthProperties.getUnitMmPattern()
                + '|' + quantityLengthProperties.getUnitCmPattern()
                + '|' + quantityLengthProperties.getUnitDmPattern();


        this.dimensionPattern = Pattern.compile(quantityLengthProperties.getDimensionAttributePattern(), Pattern.CASE_INSENSITIVE);
    }


    public MeasurementExpression normalizeMeasurementLength(MeasurementExpression measurementExpression){

        if(measurementExpression.getCode() != null) {
            BigDecimal codeFactor = getLengthNormalisationFactor(measurementExpression.getUnit(), measurementExpression.getCodeUnit());
            List<BigDecimal> normalizedValues = new ArrayList<>();
            for (BigDecimal value : measurementExpression.getValues()) {
                normalizedValues.add(value.multiply(codeFactor));
            }
            measurementExpression.setCodeValue(Collections.max(normalizedValues));
        }

        measurementExpression.setNormalizedUnit(quantityLengthProperties.getNormalizationUnit());
        BigDecimal normFactor = getLengthNormalisationFactor(measurementExpression.getUnit(), measurementExpression.getNormalizedUnit());
        List<BigDecimal> normalizedValues = new ArrayList<>();
        for (BigDecimal value : measurementExpression.getValues()) {
            normalizedValues.add(value.multiply(normFactor));
        }
        measurementExpression.setNormalizedValues(normalizedValues);
        return measurementExpression;
    }


    private BigDecimal getLengthNormalisationFactor(String measurementUnit, String normUnit){
        if(measurementUnit.equals(mm.toString()) && normUnit.equals(cm.toString()))
            return BigDecimal.valueOf(0.1);
        if(measurementUnit.equals(LengthUnit.cm.toString()) && normUnit.equals(LengthUnit.mm.toString()))
            return BigDecimal.valueOf(10);
        return BigDecimal.valueOf(1);
    }

    private MeasurementExpression setQuantitySubtype(JsonNode document, MeasurementExpression measurementExpression) throws Exception {
        int minTokenId = JsonNlpUtil.getSentenceTokenFrom(document, JsonNlpUtil.getSentenceId(document, measurementExpression.getTokens().get(0)));
        int maxTokenId = measurementExpression.getTokens().get(0);

        StringBuilder stringBuilder = new StringBuilder();
        for(int i = minTokenId; i < maxTokenId ; i++){
            JsonNode token = document.get("tokenList").get(i - 1);
            stringBuilder.append(token.get("text").textValue());
            if(token.get("misc").get("SpaceAfter").asBoolean() == true){
                stringBuilder.append(" ");
            }
        }

        String subsentence = stringBuilder.toString();
        String foundSubQuantity = null;
        int index = -1;
        for(PhysicalQuantitySubtype subtype : measurementExtractorProperties.getQuantitySubtypes()){
                if(!measurementExpression.getQuantity().equals(subtype.getQuantity().toString()))
                    continue;

                Pattern pattern = Pattern.compile(subtype.getRegex());
                Matcher matcher = pattern.matcher(subsentence);
                while (matcher.find()) {
                    int matcherIndex = matcher.start();
                    if(matcherIndex > index) {
                        foundSubQuantity = subtype.getSubtype();
                        index = matcherIndex;
                    }
                }
        }
        CodeProperties code = codeExtractionService.getCodes().get(measurementExpression.getCode());
        if(code != null){
            if(foundSubQuantity != null) {
                if (!foundSubQuantity.equals(code.getSubQuantity())) {
                    measurementExpression.setCode(null);
                }
            }
            else{
                foundSubQuantity = code.getSubQuantity();
            }
        }

        measurementExpression.setQuantitySubtype(foundSubQuantity);
        return measurementExpression;
    }


    private MeasurementExpression setMeasurementQuantities(JsonNode document, MeasurementExpression measurementExpression) {
        if(measurementExpression.getUnit().matches(unitPattern)){
            measurementExpression.setQuantity(LENGTH.toString());
        }
        try {
            setQuantitySubtype(document, measurementExpression);
        } catch (Exception e) {
            logger.error("Error setting measurement subtype", e);
            measurementExpression.setQuantitySubtype(e.getMessage());
        }
        return measurementExpression;
    }


}
