package com.github.putssander.medstruct.measurementextractor.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("measurement-extractor.quantity.length")
public class QuantityLengthProperties {

    private String unitMmPattern = "mm|millimeter";
    private String unitCmPattern = "cm|centimeter";
    private String unitDmPattern = "dm|decimeter";
    private String dimensionAttributePattern = "(x|bij|maal|op|plus.*minus|ongeveer|om en nabij|a|circa|en)";
    private String normalizationUnit = "cm";

    public String getUnitMmPattern() {
        return unitMmPattern;
    }

    public void setUnitMmPattern(String unitMmPattern) {
        this.unitMmPattern = unitMmPattern;
    }

    public String getUnitCmPattern() {
        return unitCmPattern;
    }

    public void setUnitCmPattern(String unitCmPattern) {
        this.unitCmPattern = unitCmPattern;
    }

    public String getUnitDmPattern() {
        return unitDmPattern;
    }

    public void setUnitDmPattern(String unitDmPattern) {
        this.unitDmPattern = unitDmPattern;
    }

    public String getDimensionAttributePattern() {
        return dimensionAttributePattern;
    }

    public void setDimensionAttributePattern(String dimensionAttributePattern) {
        this.dimensionAttributePattern = dimensionAttributePattern;
    }

    public String getNormalizationUnit() {
        return normalizationUnit;
    }

    public void setNormalizationUnit(String normalizationUnit) {
        this.normalizationUnit = normalizationUnit;
    }

}
