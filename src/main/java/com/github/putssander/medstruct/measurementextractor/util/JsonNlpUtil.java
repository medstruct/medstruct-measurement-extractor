package com.github.putssander.medstruct.measurementextractor.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Iterator;

public class JsonNlpUtil {


    public static JsonNode getJsonNode(String jsonNlp, ObjectMapper objectMapper) throws IOException {
        return objectMapper.readTree(jsonNlp);
    }


    public static JsonNode getDocument(JsonNode jsonNlpNode) {
        return jsonNlpNode.get("documents").get(0);
    }


    public static String refreshDocument(JsonNode jsonNlpNode, JsonNode document, ObjectMapper objectMapper) throws JsonProcessingException {
        ((ArrayNode) jsonNlpNode.withArray("documents")).set(0, document);
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNlpNode);
    }

    public static JsonNode addModuleResult(JsonNode document, JsonNode jsonNode, String key) {
        ((ObjectNode) document).put(key, jsonNode);
        return document;

    }

    public static JsonNode createJsonNode(Object object, ObjectMapper objectMapper){
        return objectMapper.valueToTree(object);
    }

    public static boolean isInSameSentence(JsonNode document, int token1, int token2) {
        return JsonNlpUtil.getSentenceId(document, token1) == (JsonNlpUtil.getSentenceId(document, token2));
    }

    public static Integer getSentenceId(JsonNode document, int token_id) {
        return document.get("tokenList").get(token_id - 1).get("sentence_id").asInt();
    }

    public static Integer getSentenceTokenFrom(JsonNode document, int sentenceId){
        return document.get("sentences").get(Integer.toString(sentenceId)).get("tokenFrom").asInt();
    }
}

