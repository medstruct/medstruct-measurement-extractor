package com.github.putssander.medstruct.measurementextractor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeasurementExtractorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeasurementExtractorApplication.class, args);
	}

}
