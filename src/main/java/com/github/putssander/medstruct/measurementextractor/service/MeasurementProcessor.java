package com.github.putssander.medstruct.measurementextractor.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.integration.annotation.Transformer;
import org.springframework.stereotype.Service;


@Service
@EnableBinding(Processor.class)
@ConditionalOnProperty(prefix="processor", name="enabled")
public class MeasurementProcessor {

    private static final Logger logger = LoggerFactory.getLogger(MeasurementProcessor.class);

    private MeasurementExtractorService measurementExtractorService;

    public MeasurementProcessor(MeasurementExtractorService measurementExtractorService) {
        logger.info("processor.enabled=true");
        this.measurementExtractorService = measurementExtractorService;
    }

    @Transformer(inputChannel = Processor.INPUT, outputChannel = Processor.OUTPUT)
    public String handle(String object) {
        logger.info("INPUT");
        String out = measurementExtractorService.extractMeasurements(object);
        logger.info("OUTPUT");
        return out;
    }

}