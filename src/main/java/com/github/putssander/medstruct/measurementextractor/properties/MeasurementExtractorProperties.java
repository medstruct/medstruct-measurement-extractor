package com.github.putssander.medstruct.measurementextractor.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ConfigurationProperties("measurement-extractor")
public class MeasurementExtractorProperties {


    private boolean processorEnabled = true;
    private boolean restEnabled = false;
    private List<CodeProperties> codes = new ArrayList<>();
    private List<PhysicalQuantitySubtype> quantitySubtypes = new ArrayList<>();
    private boolean limitCodeMatchingToSentenceStart = true;
    private String blackListPattern;
    private String splitPattern;
    private Map<String, String> ordinalMap = new HashMap<>();

    public boolean isProcessorEnabled() {
        return processorEnabled;
    }

    public void setProcessorEnabled(boolean processorEnabled) {
        this.processorEnabled = processorEnabled;
    }

    public boolean isRestEnabled() {
        return restEnabled;
    }

    public void setRestEnabled(boolean restEnabled) {
        this.restEnabled = restEnabled;
    }

    public List<CodeProperties> getCodes() {
        return codes;
    }

    public void setCodes(List<CodeProperties> codes) {
        this.codes = codes;
    }

    public List<PhysicalQuantitySubtype> getQuantitySubtypes() {
        return quantitySubtypes;
    }

    public void setQuantitySubtypes(List<PhysicalQuantitySubtype> quantitySubtypes) {
        this.quantitySubtypes = quantitySubtypes;
    }

    public String getBlackListPattern() {
        return blackListPattern;
    }

    public void setBlackListPattern(String blackListPattern) {
        this.blackListPattern = blackListPattern;
    }

    public String getSplitPattern() {
        return splitPattern;
    }

    public void setSplitPattern(String splitPattern) {
        this.splitPattern = splitPattern;
    }

    public boolean isLimitCodeMatchingToSentenceStart() {
        return limitCodeMatchingToSentenceStart;
    }

    public void setLimitCodeMatchingToSentenceStart(boolean limitCodeMatchingToSentenceStart) {
        this.limitCodeMatchingToSentenceStart = limitCodeMatchingToSentenceStart;
    }

    public Map<String, String> getOrdinalMap() {
        return ordinalMap;
    }

    public void setOrdinalMap(Map<String, String> ordinalMap) {
        this.ordinalMap = ordinalMap;
    }
}
