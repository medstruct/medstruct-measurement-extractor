package com.github.putssander.medstruct.measurementextractor.properties;

import com.github.putssander.medstruct.measurementextractor.enumeration.PhysicalQuantityType;

public class CodeProperties {

    private String code;

    private String unit;

    private String precedingConcept;

    private PhysicalQuantityType physicalQuantityType;

    private String subQuantity;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrecedingConcept() {
        return precedingConcept;
    }

    public void setPrecedingConcept(String precedingConcept) {
        this.precedingConcept = precedingConcept;
    }

    public PhysicalQuantityType getPhysicalQuantityType() {
        return physicalQuantityType;
    }

    public void setPhysicalQuantityType(PhysicalQuantityType physicalQuantityType) {
        this.physicalQuantityType = physicalQuantityType;
    }

    public String getSubQuantity() {
        return subQuantity;
    }

    public void setSubQuantity(String subQuantity) {
        this.subQuantity = subQuantity;
    }
}
