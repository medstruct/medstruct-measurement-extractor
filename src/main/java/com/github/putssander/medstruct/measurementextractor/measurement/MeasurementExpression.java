package com.github.putssander.medstruct.measurementextractor.measurement;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MeasurementExpression {


    private int id;

    private String expression;

    private List<Integer> tokens = new ArrayList<>();

    private List<Integer> codeTokens = new ArrayList<>();

    private String unit;

    private String quantity;

    private String quantitySubtype;

    private List<BigDecimal> values = new ArrayList<>();

    private String normalizedUnit;

    private List<BigDecimal> normalizedValues = new ArrayList<>();

    private String code;

    private String codeUnit;

    private BigDecimal codeValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public List<Integer> getTokens() {
        return tokens;
    }

    public void setTokens(List<Integer> tokens) {
        this.tokens = tokens;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantitySubtype() {
        return quantitySubtype;
    }

    public void setQuantitySubtype(String quantitySubtype) {
        this.quantitySubtype = quantitySubtype;
    }

    public List<BigDecimal> getValues() {
        return values;
    }

    public void setValues(List<BigDecimal> values) {
        this.values = values;
    }

    public String getNormalizedUnit() {
        return normalizedUnit;
    }

    public void setNormalizedUnit(String normalizedUnit) {
        this.normalizedUnit = normalizedUnit;
    }

    public List<BigDecimal> getNormalizedValues() {
        return normalizedValues;
    }

    public void setNormalizedValues(List<BigDecimal> normalizedValues) {
        this.normalizedValues = normalizedValues;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeUnit() {
        return codeUnit;
    }

    public void setCodeUnit(String codeUnit) {
        this.codeUnit = codeUnit;
    }

    public BigDecimal getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(BigDecimal codeValue) {
        this.codeValue = codeValue;
    }

    public List<Integer> getCodeTokens() {
        return codeTokens;
    }

    public void setCodeTokens(List<Integer> codeTokens) {
        this.codeTokens = codeTokens;
    }
}