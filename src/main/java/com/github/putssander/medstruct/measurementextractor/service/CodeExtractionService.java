package com.github.putssander.medstruct.measurementextractor.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.putssander.medstruct.measurementextractor.measurement.MeasurementExpression;
import com.github.putssander.medstruct.measurementextractor.properties.CodeProperties;
import com.github.putssander.medstruct.measurementextractor.properties.MeasurementExtractorProperties;
import com.github.putssander.medstruct.measurementextractor.properties.QuantityLengthProperties;
import com.github.putssander.medstruct.measurementextractor.util.JsonNlpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@EnableConfigurationProperties(MeasurementExtractorProperties.class)
public class CodeExtractionService {

    private static final Logger logger = LoggerFactory.getLogger(CodeExtractionService.class);

    private MeasurementExtractorProperties measurementExtractorProperties;

    private Map<String, CodeProperties> codesPrecededBy = new HashMap<>();

    private Map<String, CodeProperties> codes = new HashMap<>();


    public CodeExtractionService(MeasurementExtractorProperties measurementExtractorProperties) {
        this.measurementExtractorProperties = measurementExtractorProperties;
        initCodeMaps();
    }

    public Map<String, CodeProperties> getCodes() {
        return codes;
    }

    private void initCodeMaps() {
        codesPrecededBy = measurementExtractorProperties.getCodes().stream()
                .collect(Collectors.toMap(CodeProperties::getPrecedingConcept, code -> code));
        codes = measurementExtractorProperties.getCodes().stream()
                .collect(Collectors.toMap(CodeProperties::getCode, code -> code));
    }

    public MeasurementExpression getMeasurementCode(MeasurementExpression measurementExpression, JsonNode document){
        List<JsonNode> tokenList = new ArrayList<>();
        document.get("tokenList").elements().forEachRemaining(tokenList::add);
        int maxTokenId = measurementExpression.getTokens().get(0) - 1;
        int minTokenId = 1;
        if(measurementExtractorProperties.isLimitCodeMatchingToSentenceStart()){
            int sentenceId = JsonNlpUtil.getSentenceId(document, measurementExpression.getTokens().get(0).intValue());
            minTokenId = JsonNlpUtil.getSentenceTokenFrom(document, sentenceId);
            if(maxTokenId <= minTokenId)
                return measurementExpression;
        }

        for(int i = maxTokenId; i >= minTokenId; i--){
            JsonNode token = tokenList.get(i-1);

            if(isOntologyEntity(token)){

                String uri = getOntologyUri(token);
                if(codesPrecededBy.containsKey(uri)){
                    CodeProperties code = codesPrecededBy.get(uri);
                    setCodeProperties(measurementExpression, code, token);
                    return measurementExpression;
                }
            }

        }

        return measurementExpression;
    }





    private boolean isOntologyEntity(JsonNode token){
        return token.has("entity") && token.get("entity").textValue().equals("ONTOLOGY");
    }

    private String getOntologyUri(JsonNode token){
        return token.get("misc").get("uri").get(0).textValue();
    }

    private MeasurementExpression setCodeProperties(MeasurementExpression measurementExpression, CodeProperties codeProperties, JsonNode token){
        measurementExpression.setCode(codeProperties.getCode());
        measurementExpression.setCodeUnit(codeProperties.getUnit());
        measurementExpression.setCodeTokens(Arrays.asList(token.get("id").asInt()));
        return measurementExpression;
    }

}
